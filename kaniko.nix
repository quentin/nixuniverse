pkgs:
pkgs.buildGoModule rec {
  pname = "kaniko";
  version = "1.9.1";

  src = pkgs.fetchFromGitHub {
    owner = "GoogleContainerTools";
    repo = "kaniko";
    rev = "v${version}";
    sha256 = "sha256-sPICsDgkijQ7PyeTWQgT553toc4/rWPPo7SY3ptX82U=";
  };

  vendorSha256 = null;

  checkPhase = "true";

  meta = with pkgs.lib; {
    description =
      "kaniko is a tool to build container images from a Dockerfile, inside a container or Kubernetes cluster.";
    homepage = "https://github.com/GoogleContainerTools/kaniko";
    license = licenses.asl20;
    platforms = platforms.linux;
  };
}
